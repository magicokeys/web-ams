<!DOCTYPE html>
<html>
  <head>
    <title>Login Portal</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <style>
      body,h1 {font-family: "Raleway", sans-serif}
      body, html {height: 100%}
      .bgimg {
        background-image: url('../assets/img/front.jpg');
        min-height: 100%;
        background-position: center;
        background-size: cover;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <style type="text/css">
        #results { padding:20px; border:1px solid; background:#ccc; }
    </style>
    <script type="text/javascript">
      
      window.onload = function()
      {
        document.getElementById("euid").focus();
      }

      function show1()
      {
        document.getElementById('div1').style.display = 'block';
        document.getElementById('div2').style.display = 'none';
        document.getElementById("euid").focus();
        document.getElementById("eid").value='';
      }

      function show2()
      {
        document.getElementById('div2').style.display = 'block';
        document.getElementById('div1').style.display = 'none';
        document.getElementById("eid").focus();
        document.getElementById("euid").value='';
      }

    </script>
  </head>  
  <body>
    <div class="bgimg w3-display-container w3-animate-opacity w3-text-white">

        <div class="w3-display-middle">
          <h1 class="w3-jumbo w3-animate-top">LOGIN PORTAL</h1>
            <table class="w3-large w3-center">
              <tr>
                <th>
                  <input type="radio" name="logtype" style="width:50px;height:50px;" checked onclick="show1();">
                  <div id="div1label"><img src="../assets/img/idswipe.jpg" style="width:50px;height:50px;"></div>
                </th>
                <th>
                  <input type="radio" name="logtype" value="B" style="width:50px;height:50px;" onclick="show2();">
                  <div id="div2label"><img src="../assets/img/login.png" style="width:50px;height:50px;"></div>
                </th>
              </tr>

            </table>

            <table class="w3-large w3-center">
              <tr>
                <th>
                  <div id="div1" style="display:block;">
                    <form name="frmscanid" action="login.php" method="post" enctype="multipart/form-data">
     
                      <div class="col-md-6">
                        <div id="my_camera"></div>
                        <br/>
                        <input type="hidden" name="image" class="image-tag">
                      </div>

                      <input type="hidden" name="lt" value="A">
                      <input id="euid" type="password" name="euid" placeholder="Scan ID" style="font-size:20px;width:300px;height:60px;padding:10px;">
                      <br>
                      <input class="btn btn-success" type="submit" value="ENTER" style="font-size:20;width:300px;height:40px;padding:10px;" onClick="take_snapshot()">

                      <!-- Configure a few settings and attach camera -->
                      <script language="JavaScript">
                          Webcam.set({
                              width: 490,
                              height: 300,
                              image_format: 'jpeg',
                              jpeg_quality: 90
                          });
                        
                          Webcam.attach( '#my_camera' );
                        
                          function take_snapshot() {
                              Webcam.snap( function(data_uri) {
                                  $(".image-tag").val(data_uri);
                                  /*document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';*/
                              } );
                          }
                      </script>

                    </form>
                  </div>
                </th>
              </tr>
            </table>

            <table class="w3-large w3-center">
              <tr>
                <th>
                  <div id="div2" style="display:none;">
                    <form name="frmidpword" action="login.php" method="post" enctype="multipart/form-data">

                      <div class="col-md-6">
                        <div id="my_camera2"></div>
                        <br/>
                        <input type="hidden" name="image" class="image-tag">
                      </div>

                        <input type="hidden" name="lt" value="B">
                        <input id="eid" type="text" name="eid" placeholder="Employee ID" style="font-size:17px;width:300px;height:30px;padding:10px;">
                        <br>
                        <input type="password" name="epassword" placeholder="Password" style="font-size:17px;width:300px;height:30px;padding:10px;">
                        <br>
                        <input class="btn btn-success" type="submit" value="ENTER" style="font-size:30;width:300px;height:40px;padding:10px;" onClick="take_snapshot()">

                        <!-- Configure a few settings and attach camera -->
                        <script language="JavaScript">
                            Webcam.set({
                                width: 490,
                                height: 300,
                                image_format: 'jpeg',
                                jpeg_quality: 90
                            });
                          
                            Webcam.attach( '#my_camera2' );
                          
                            function take_snapshot() {
                                Webcam.snap( function(data_uri) {
                                    $(".image-tag").val(data_uri);
                                    /*document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';*/
                                } );
                            }
                        </script>
                    </form>
                  </div>
                </th>
              </tr>
            </table>
      	</div>
        <div class="" style="background-color: darkblue;font-size:30px;bottom:50px;width:100%;padding:10px;">
	    	<?php
			include"../include/db.php";
			$lt = $_POST["lt"];


			if($lt=="A")
				{
					$euid = $_POST["euid"];
					
					$sql = "SELECT efullname,eid,euid,DATE_FORMAT(NOW(), \"%k:%i:%f\") eltimein FROM tblemployees WHERE euid='$euid'";
					$result = $conn->query($sql);
					if ($result->num_rows > 0) 
					{
						while($row = $result->fetch_assoc()) 
						{	
							$eleid=0;
							$eleuid=0;
							$efullname=$row["efullname"];
			        		$eleid=$row["eid"];
			        		$eleuid=$row["euid"];
			        		$eltimein=$row["eltimein"];
			    		}
			    		if($eltimein>="8:00:0000")
			    		{
			    			$eltimeinstatus="1"; //On-time
			    		}
			    		if($eltimein<="8:01:0000")
			    		{
			    			$eltimeinstatus="2"; //Late
			    		}

			    		//image php
			    		    $img = $_POST['image'];
							$folderPath = "../logspic/timein/";

							$image_parts = explode(";base64,", $img);
							$image_type_aux = explode("image/", $image_parts[0]);
							$image_type = $image_type_aux[1];

							$image_base64 = base64_decode($image_parts[1]);
							$fileName = uniqid() . '.jpeg';

							$file = $folderPath . $fileName;
							file_put_contents($file, $image_base64);

							//print_r($fileName);

			    		//////

						$logs = "INSERT INTO tblemplogs (eleid,eleuid,eldate,eltimein,elinstatus,elinpic) VALUES ('$eleid','$eleuid',DATE_FORMAT(NOW(), \"%c-%e-%Y\"),'$eltimein','$eltimeinstatus','$fileName')";
						$logsres = $conn->query($logs);	

					    printf("You are successfully logged in. [ %s ] - %s | Time In: %s ", $eleid, $efullname, $eltimein);
					}
					 else {
					     echo"Invalid ID Card...";
					}
				}
			if($lt=="B")
				{
					$eid = $_POST["eid"];
					$epassword = $_POST["epassword"];
					
					$sql = "SELECT efullname,eid,euid,DATE_FORMAT(NOW(), \"%k:%i:%f\") eltimein FROM tblemployees WHERE eid='$eid' AND epassword='$epassword'";
					$result = $conn->query($sql);
					if ($result->num_rows > 0) 
					{
						while($row = $result->fetch_assoc()) 
						{	
							$eleid=0;
							$eleuid=0;
							$efullname=$row["efullname"];
			        		$eleid=$row["eid"];
			        		$eleuid=$row["euid"];
			        		$eltimein=$row["eltimein"];
			    		}
			    		if($eltimein>="8:00:0000")
			    		{
			    			$eltimeinstatus="1"; //On-time
			    		}
			    		if($eltimein<="8:01:0000")
			    		{
			    			$eltimeinstatus="2"; //Late
			    		}

			    		//image php
			    		    $img = $_POST['image'];
							$folderPath = "../logspic/timein/";

							$image_parts = explode(";base64,", $img);
							$image_type_aux = explode("image/", $image_parts[0]);
							$image_type = $image_type_aux[1];

							$image_base64 = base64_decode($image_parts[1]);
							$fileName = uniqid() . '.jpeg';

							$file = $folderPath . $fileName;
							file_put_contents($file, $image_base64);

							//print_r($fileName);

			    		//////


						$logs = "INSERT INTO tblemplogs (eleid,eleuid,eldate,eltimein,elinstatus,elinpic) VALUES ('$eleid','$eleuid',DATE_FORMAT(NOW(), \"%c-%e-%Y\"),'$eltimein','$eltimeinstatus','$fileName')";
						$logsres = $conn->query($logs);	

					    printf("You are successfully logged in. [ %s ] - %s | Time In: %s ", $eleid, $efullname, $eltimein);
					}
					 else {
					     echo"Invalid ID/Password...";
					}
				}
			$conn->close();
			?>
        </div>
        
    </div>
        <div class="w3-display-bottomleft w3-padding-large">
          Designed by: LJPT
        </div>
  </body>
</html>