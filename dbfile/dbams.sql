-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2019 at 07:04 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbams`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblemplogs`
--

CREATE TABLE `tblemplogs` (
  `id` int(11) NOT NULL,
  `eleid` varchar(10) NOT NULL,
  `eleuid` varchar(20) NOT NULL,
  `eldate` varchar(10) NOT NULL,
  `eltimein` varchar(10) NOT NULL,
  `eltimeout` varchar(10) NOT NULL,
  `elinstatus` varchar(5) NOT NULL,
  `eloutstatus` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblemplogs`
--

INSERT INTO `tblemplogs` (`id`, `eleid`, `eleuid`, `eldate`, `eltimein`, `eltimeout`, `elinstatus`, `eloutstatus`) VALUES
(1, '101', '12345', '10-3-2019', '2019-10-03', '', '1', ''),
(2, '101', '12345', '10-3-2019', '11:51:0000', '', '1', ''),
(3, '101', '12345', '10-3-2019', '', '', 'eltim', ''),
(4, '101', '12345', '10-3-2019', '11:56:0000', '', 'eltim', ''),
(5, '101', '12345', '10-3-2019', '11:56:0000', '', '1', ''),
(6, '101', '12345', '10-3-2019', '11:57:0000', '', '1', ''),
(7, '101', '12345', '10-3-2019', '11:57:0000', '', '', ''),
(8, '101', '12345', '10-3-2019', '11:57:0000', '', '2', ''),
(9, '101', '12345', '10-3-2019', '12:02:0000', '', '2', ''),
(10, '102', '56789', '10-3-2019', '12:02:0000', '', '2', ''),
(11, '101', '12345', '10-3-2019', '12:56:0000', '', '2', ''),
(12, '101', '12345', '10-3-2019', '12:57:0000', '', '2', ''),
(13, '101', '12345', '10-3-2019', '12:57:0000', '', '2', ''),
(14, '101', '12345', '10-3-2019', '12:57:0000', '', '2', ''),
(15, '101', '12345', '10-3-2019', '12:58:0000', '', '2', ''),
(16, '101', '12345', '10-3-2019', '12:58:0000', '', '2', ''),
(17, '101', '12345', '10-3-2019', '12:59:0000', '', '2', ''),
(18, '101', '12345', '10-3-2019', '12:59:0000', '', '2', ''),
(19, '101', '12345', '10-3-2019', '12:59:0000', '', '2', ''),
(20, '101', '12345', '10-3-2019', '13:00:0000', '', '2', ''),
(21, '101', '12345', '10-3-2019', '13:00:0000', '', '2', ''),
(22, '101', '12345', '10-3-2019', '13:00:0000', '', '2', '');

-- --------------------------------------------------------

--
-- Table structure for table `tblemployees`
--

CREATE TABLE `tblemployees` (
  `id` int(11) NOT NULL,
  `eid` varchar(10) NOT NULL,
  `efullname` varchar(50) NOT NULL,
  `epassword` varchar(10) NOT NULL,
  `euid` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblemployees`
--

INSERT INTO `tblemployees` (`id`, `eid`, `efullname`, `epassword`, `euid`) VALUES
(1, '101', 'Luis Endrado', 'luis', '12345'),
(2, '102', 'James Adragat', 'james', '56789');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblemplogs`
--
ALTER TABLE `tblemplogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblemployees`
--
ALTER TABLE `tblemployees`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblemplogs`
--
ALTER TABLE `tblemplogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tblemployees`
--
ALTER TABLE `tblemployees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
